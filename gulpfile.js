//PLUGINS
var {src, dest, series, parallel, watch, lastRun} = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var csscomb = require('gulp-csscomb');
var cleanCSS = require('gulp-clean-css');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var imagemin = require('gulp-imagemin');
var browserSync = require('browser-sync').create();


//PROXY
var proxy = 'localhost/maquettes/casaroyal-real-estate-homepage-alex-gurghis/app';


//FILES HTML PHP
var filesPHP = 'app/**/*.html';


//FILES PUBLIC
var source = 'src/'; 
var sourceStyles = source + 'css/';
var sourceStylesFilesSCSS = sourceStyles + '*.scss';
var sourceStylesFilesCSS = sourceStyles + '*.css';
var sourceJS = source + 'js/';
var sourceJSFilesJS = sourceJS + '*.js';
var sourceImg = source + 'img/';
var sourceImgFilesImg = sourceImg + '*.{png,jpg,jpeg,gif,svg}';

var destination = 'dist/';
var destinationStyles = destination + 'css/';
var destinationJS = destination + 'js/';
var destinationImg = destination + 'img/';


//CSS
function scssToCss ()
{
	return src(sourceStylesFilesSCSS)
		.pipe(sass())
		.pipe(csscomb())
		.pipe(dest(sourceStyles));
}

function minifyCss ()
{
	return src(sourceStylesFilesCSS)
		.pipe(cleanCSS())
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(dest(destinationStyles));
}


//JS
function minifyJS ()
{
	return src(sourceJSFilesJS)
		.pipe(uglify())
		.pipe(concat('app.min.js'))
		.pipe(dest(destinationJS));
}


//IMG
function minifyImg ()
{
	return src(sourceImgFilesImg, {since: lastRun(minifyImg)})
		.pipe(imagemin())
		.pipe(dest(destinationImg));
}


//SERVE
function serve ()
{
	browserSync.init({
        proxy: proxy
    });

    watch(filesPHP).on('change', browserSync.reload);
    watch(sourceStylesFilesSCSS, series(scssToCss, minifyCss)).on('change', browserSync.reload);
    watch(sourceJSFilesJS, minifyJS).on('change', browserSync.reload);
    watch(sourceImgFilesImg, minifyImg).on('change', browserSync.reload);
}


//EXPORT
module.exports = 
{
	styles: series(scssToCss, minifyCss),
	js: minifyJS,
	img: minifyImg,
	prod: parallel(series(scssToCss, minifyCss), minifyJS, minifyImg),
	serve: serve
}